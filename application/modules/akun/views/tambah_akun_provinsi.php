<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Tambah Admin Provinsi</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Admin Provinsi</a></li>
                <li class="breadcrumb-item active">Tambah Admin Provinsi</li>
            </ol> 
            <a href="<?php echo site_url('akun/daftar_admin_provinsi'); ?>"> <button type="button" class="btn btn-warning d-none d-lg-block m-l-15"><i class="fa fa-list-ol"></i> Daftar Admin Provinsi</button></a> 
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <!-- Column -->
        <div class="col-lg-12 col-md-12">
            <?php echo $this->session->flashdata('flash_message'); ?>
        </div>
        <!-- Column -->
        <div class="card">
            <div class="card card-body">
                <h4 class="card-title">Formulir Tambah Admin Provinsi</h4>
                <h6 class="card-subtitle"> Silahkan mengisi formulir tambah admin provinsi </h6>
                <form class="form-horizontal m-t-20 row" action="<?php echo site_url('akun/kirim_admin_provinsi'); ?>" enctype="multipart/form-data" method="post">          
                    <div class="form-group col-md-6 m-t-10" >
                        <label>Nama Lengkap Admin Provinsi <span class="text-danger">*</span></label>
                        <input type="text" name="nama_admin" class="form-control" placeholder="Isikan nama lengkap admin provinsi" required data-validation-required-message="Kolom ini wajib diisi">
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>
                    <div class="form-group col-md-3 m-t-10">
                        <label>Hak Akses Admin Provinsi <span class="text-danger">*</span></label>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="create" value="1" checked="" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Create</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="read" value="1" checked class="custom-control-input" id="customCheck2" disabled="" >
                            <label class="custom-control-label" for="customCheck2">Read</label>
                        </div>          
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>
                    <div class="form-group col-md-3 m-t-10">
                        <label></label>                        
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="update" value="1" checked="" class="custom-control-input" id="customCheck3">
                            <label class="custom-control-label" for="customCheck3">Update</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="delete"value="1" checked="" class="custom-control-input" id="customCheck4">
                            <label class="custom-control-label" for="customCheck4">Delete</label>
                        </div>
                    </div>                   
                    <div class="form-group col-md-6 m-t-10" >
                        <label>Nama Email Admin Provinsi <span class="text-danger">*</span></label>
                        <input type="email" name="email" class="form-control" placeholder="Isikan email admin provinsi" required data-validation-required-message="Kolom ini wajib diisi">
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>
                    <div class="form-group col-md-6 m-t-10">
                        <label>Nomor HP Admin Provinsi <span class="text-danger">*</span></label>
                        <div class="input-group">                            
                            <input type="number" name="nomor_hp" class="form-control" placeholder="Isikan nomor HP admin provinsi" required data-validation-required-message="Kolom ini wajib diisi">
                            <div class="input-group-append">
                                <span class="input-group-text">Angka</span>
                            </div>
                        </div>
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>
                    <div class="form-group col-md-2 m-t-5">
                        <label>Status Admin Provinsi<span class="text-danger">*</span></label>
                        <div class="custom-control custom-radio m-t-10">
                            <input type="radio" id="sts1" value="1" name="status" class="custom-control-input">
                            <label class="custom-control-label" for="sts1">Aktif</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="sts2" value="0" name="status" class="custom-control-input">
                            <label class="custom-control-label" for="sts2">Tidak Aktif</label>
                        </div>
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>
                    <div class="form-group col-md-5 m-t-10">
                        <label>Pilih Wilayah/Regional Admin <span class="text-danger">*</span></label>
                        <select name="provinsi" class="select2 form-control custom-select" style="width: 100%; height:36px;" id="provinsi" required>
                            <option>Pilih Provinsi</option>
                            <?php
                            if (!empty($provinsi)) {
                                foreach ($provinsi as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?></option>                                     
                                    <?php
                                }
                            }
                            ?>                       
                        </select>
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>                     
                    <div class="form-group col-md-5 m-t-10">
                        <label>License Key Expired <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input type="text" name="license_exp" class="form-control" placeholder="Tanggal Expired" id="license_date" required data-validation-required-message="Kolom ini wajib diisi!">                          
                        </div>
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>
                    <div class="form-group col-md-12 m-t-5">
                        <label>License Key (base 64) <span class="text-danger">*</span></label>
                        <div class="input-group"> 
                            <textarea class="form-control" rows="5" name="license" ></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-6 m-t-10">
                        <label>Input Password <span class="text-danger">*</span></label>                   
                        <input type="password" name="password" class="form-control" placeholder="Inputkan password" required data-validation-required-message="Kolom ini wajib diisi" >                        
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>  
                    <div class="form-group col-md-6 m-t-10">
                        <label>Input Konfirmasi Password <span class="text-danger">*</span></label>                       
                        <input type="password" name="cf_passwd" class="form-control" data-validation-match-match="password" placeholder="Inputkan konfirmasi password" required data-validation-required-message="Kolom ini wajib diisi" >                       
                        <small class="form-control-feedback">*Kolom Ini Harus <b>Diisi</b> ! </small>
                    </div>
                    <div class="form-group col-md-12 m-t-10">
                        <label>Foto Admin <span class="text-danger">*</span></label>
                        <input type="file" name="img" class="form-control" required data-validation-required-message="Kolom ini wajib diisi" aria-invalid="false">
                        <small class="form-control-feedback">*Gunakan Ukuran File Kurang Dari <b>2 MB</b> ! </small>
                    </div> 
                    <div class="form-group col m-t-10">
                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                        <button type="reset" onclick="history.back()" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- .row -->
<!-- Plugin JavaScript -->
