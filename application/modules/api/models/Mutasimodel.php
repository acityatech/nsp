<?php

class Mutasimodel extends CI_Model {

    private $mutasi = 'mutasi';

    /**
     * METHOD/FUNCTION USER CRUD
     */
    public function get_all_mutasi($id_tujuan = '', $limit = '') {


        $sql = $this->db->query("SELECT
                                        t.*
                                    FROM
                                        (
                                        SELECT
                                            m.*,
                                            wpasl.nama AS provinsi_asal,
                                            wkbasl.nama AS kabupaten_asal,
                                            wptj.nama AS provinsi_tujuan,
                                            wkbtj.nama AS kabupaten_tujuan
                                        FROM
                                            mutasi m
                                        LEFT JOIN wilayah_kabupaten wkbtj ON
                                            SUBSTR(m.id_region_tujuan, 3, 2) = wkbtj.id AND SUBSTR(m.id_region_tujuan, 1, 2) = wkbtj.id_dati1
                                        LEFT JOIN wilayah_provinsi wptj ON
                                            SUBSTR(m.id_region_tujuan, 1, 2) = wptj.id
                                        LEFT JOIN wilayah_kabupaten wkbasl ON
                                            SUBSTR(m.id_region_asal, 3, 2) = wkbasl.id AND SUBSTR(m.id_region_asal, 1, 2) = wkbasl.id_dati1
                                        LEFT JOIN wilayah_provinsi wpasl ON
                                            SUBSTR(m.id_region_asal, 1, 2) = wpasl.id
                                    ) t
                                    WHERE
                                        t.id_region_tujuan = '$id_tujuan'
                                    ORDER BY
                                        t.tgl_pengajuan_mutasi 
                                    DESC  
                                    LIMIT $limit");

        return $sql->result();
    }

    public function count_mutasi_all($id_tujuan = '') {

        $this->db->where('id_region_tujuan', $id_tujuan);
        $sql = $this->db->get($this->mutasi);
        return $sql->num_rows();
    }

}
