<?php

class Pekerjaanmodel extends CI_Model {

    private $pekerjaan = 'pekerjaan';

    /**
     * METHOD/FUNCTION JOBS CRUD
     */
    public function get_pekerjaan() {

        $this->db->select('*');

        $sql = $this->db->get($this->pekerjaan);
        return $sql->result();
    }

}
