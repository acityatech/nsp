<?php

class Berandamodel extends CI_Model {

    /**
     * GET DATA BERANDA
     */
    public function get_beranda_petugas($id = '') {

        $sql = $this->db->query("SELECT
                                    p.id_petugas,
                                    p.nama_petugas,
                                    p.id_admin,
                                    p.status_data,
                                    p.nomor_ktp,
                                    p.email_petugas,
                                    p.nomor_hp,
                                    p.alamat_petugas,
                                    p.kode_petugas,
                                    p.img,
                                    p.img_thumb,
                                    p.tanggal_post,
                                    (
                                    SELECT
                                        COUNT(k.id_ktp)
                                    FROM
                                        ktp k
                                    WHERE
                                        k.id_petugas = p.id_petugas AND k.status_data = 1
                                ) AS jml_ktp
                                FROM
                                    petugas p
                                WHERE
                                    p.id_petugas = '$id' AND p.status_data = 1");
        return $sql->result_array();
    }

    public function get_beranda_admin($id_admin = '', $role = '') {

        if (!empty($id_admin) && ($role == 2)) {

            $sql = $this->db->query("SELECT
                                        t.*
                                    FROM
                                        (
                                        SELECT
                                            (
                                            SELECT
                                                COUNT(k.id_ktp)
                                            FROM
                                                ktp k
                                            WHERE
                                                k.status_data = 1 AND k.id_admin = '$id_admin'
                                        ) AS jml_ktp,
                                        (
                                        SELECT
                                            COUNT(u.id_ref)
                                        FROM
                                            user u
                                        WHERE
                                            u.id_ref = '$id_admin'
                                    ) AS jml_reg,
                                    (
                                        SELECT
                                            COUNT(p.id_petugas)
                                        FROM
                                            petugas p
                                        WHERE
                                            p.status_data = 1 AND p.id_admin = '$id_admin'
                                    ) AS jml_pet
                                    ) t");
        } elseif (!empty($id_admin) && ($role == 1)) {

            $sql = $this->db->query("SELECT
                                        t.*
                                    FROM
                                        (
                                        SELECT
                                            (
                                            SELECT
                                                COUNT(k.id_ktp)
                                            FROM
                                                ktp k
                                            WHERE
                                                k.status_data = 1 AND k.id_admin LIKE '$id_admin%'
                                        ) AS jml_ktp,
                                        (
                                        SELECT
                                            COUNT(u.id_ref)
                                        FROM
                                            user u
                                        WHERE
                                            u.id_ref LIKE '$id_admin%'
                                    ) AS jml_reg,
                                    (
                                        SELECT
                                            COUNT(p.id_petugas)
                                        FROM
                                            petugas p
                                        WHERE
                                            p.status_data = 1 AND p.id_admin LIKE '$id_admin%'
                                    ) AS jml_pet
                                    ) t");
        } elseif ($role == 0) {

            $sql = $this->db->query("SELECT
                                        t.*
                                    FROM
                                        (
                                        SELECT
                                            (
                                            SELECT
                                                COUNT(k.id_ktp)
                                            FROM
                                                ktp k                                           
                                        ) AS jml_ktp,
                                        (
                                        SELECT
                                            COUNT(u.id_ref)
                                        FROM
                                            user u                                                                        
                                    ) AS jml_reg,
                                    (
                                        SELECT
                                            COUNT(p.id_petugas)
                                        FROM
                                            petugas p                                        
                                    ) AS jml_pet
                                    ) t");
        }
        return $sql->result();
    }

}
