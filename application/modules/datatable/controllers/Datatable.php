<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Datatable extends MX_Controller {

    public function __construct() {
        parent::__construct();
        //Do your magic here 
        if ($this->session->userdata('ktpapps') == FALSE) {
            redirect('auth');
        }
        $this->load->model('Datatablemodel');
        $this->load->library('datatables');
        $this->user = $this->session->userdata("ktpapps");
    }

    //-----------------------------------------------KTP--------------------------------------------------//

    public function ktp_json() {

        if (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 2)) {
            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir,                                    
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas AND p.id_admin = '" . $this->user['id_ref'] . "' AND p.status_data = 1 AND pt.status_data = 1 AND pt.id_admin = '" . $this->user['id_ref'] . "'", 'left');
            $this->datatables->where('p.id_admin', $this->user['id_ref']);
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            $this->datatables->add_column('view_button', $this->button_ktp($this->user), 'id_ktp, status_mutasi');
        } elseif (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 1)) {

            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    p.nama_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir,                                
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas AND p.id_admin LIKE '" . $this->user['id_ref'] . "%' AND p.status_data = 1 AND pt.status_data = 1 AND pt.id_admin LIKE '" . $this->user['id_ref'] . "%'", 'left');
            $this->datatables->like('p.id_admin', $this->user['id_ref'], 'after');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            $this->datatables->add_column('view_button', $this->button_ktp($this->user), 'id_ktp, status_mutasi');
        } elseif ($this->user['role_admin'] == 0) {

            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    p.nama_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir, 
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas", 'left');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            $this->datatables->add_column('view_button', $this->button_ktp($this->user), 'id_ktp, status_mutasi');
        }
        return print_r($this->datatables->generate());
    }

    public function ktp_reg_json($id_admin = '') {

        $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir,                                    
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
        $this->datatables->from('ktp p');
        $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
        $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
        $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
        $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
        $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
        $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
        $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas AND pt.status_data = 1 AND pt.id_admin = '" . $this->user['id_ref'] . "'", 'left');
        $this->datatables->where('p.id_admin', $id_admin);
        $this->datatables->where('p.status_data', '1');
        $this->datatables->order_by('p.tanggal_post DESC');

        $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
        $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
        $this->datatables->add_column('view_button', $this->button_reg_ktp($this->user), 'id_ktp, status_mutasi');

        return print_r($this->datatables->generate());
    }

    public function ktp_pet_json($id_pet = '') {

        $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir,                                    
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
        $this->datatables->from('ktp p');
        $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
        $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
        $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
        $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
        $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
        $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
        $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas AND pt.status_data = 1 AND pt.id_petugas = '$id_pet'");
        $this->datatables->where('p.id_petugas', $id_pet);
        $this->datatables->where('p.status_data', '1');
        $this->datatables->order_by('p.tanggal_post DESC');

        $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
        $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
        $this->datatables->add_column('view_button', $this->button_pet_ktp($this->user), 'id_ktp, status_mutasi');

        return print_r($this->datatables->generate());
    }

    public function button_ktp($usr = '') {

        $get_button = "";

        if ($usr['role_admin'] == 0) {
            
        } else {
            if ($usr['delete_prev'] == 1) {
                $get_button .= '<a onclick="act_del_ktp($1)" ><button type="button" class="btn btn-danger btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Hapus Anggota"><i class="ti-close" aria-hidden="true"></i></button></a>';
            } else {
                $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip " data-original-title="Hapus KTP NonAktif (*hub petugas)"><i class="ti-close" aria-hidden="true"></i></button>';
            }
        }
        if ($usr['update_prev'] == 1) {
            $get_button .= '<a href="' . site_url('ktp/get_ktp/') . '$1"><button type="button" class="btn btn-info btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Edit Anggota"><i class="ti-pencil" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Edit KTP NonAktif (*hub petugas)"><i class="ti-pencil" aria-hidden="true"></i></button>';
        }
        if ($usr['update_prev'] == 1 or $usr['create_prev'] == 1) {
            $get_button .= '<a target="_blank" href="' . site_url('laporan/cetak_kta/') . '$1"><button type="button" class="btn btn-warning btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Cetak KTA Anggota"><i class="ti-printer" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Cetak KTA NonAktif (*hub petugas)"><i class="ti-printer" aria-hidden="true"></i></button>';
        }
        if ($usr['update_prev'] == 1 or $usr['create_prev'] == 1) {

            $get_button .= '<a onclick="mutasi_anggota($1,$2)" ><button type="button" class="btn btn-primary btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Mutasi Anggota"><i class="ti-shift-right" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Mutasi Anggota NonAktif (*hub petugas)"><i class="ti-printer" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

    public function button_reg_ktp($usr = '') {

        $get_button = "";

        if ($usr['role_admin'] == 0) {
            
        } else {
            if ($usr['delete_prev'] == 1) {
                $get_button .= '<a onclick="act_del_ktp($1)" ><button type="button" class="btn btn-danger btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Hapus Anggota"><i class="ti-close" aria-hidden="true"></i></button></a>';
            } else {
                $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Hapus KTP NonAktif (*hub petugas)"><i class="ti-close" aria-hidden="true"></i></button>';
            }
        }
        if ($usr['update_prev'] == 1) {
            $get_button .= '<a href="' . site_url('ktp/get_ktp_admin/') . '$1"><button type="button" class="btn btn-info btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Edit Anggota"><i class="ti-pencil" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Edit KTP NonAktif (*hub petugas)"><i class="ti-pencil" aria-hidden="true"></i></button>';
        }
        if ($usr['update_prev'] == 1 or $usr['create_prev'] == 1) {
            $get_button .= '<a target="_blank" href="' . site_url('laporan/cetak_kta/') . '$1"><button type="button" class="btn btn-warning btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Cetak KTA Anggota"><i class="ti-printer" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Cetak KTA NonAktif (*hub petugas)"><i class="ti-printer" aria-hidden="true"></i></button>';
        }
        if ($usr['update_prev'] == 1 or $usr['create_prev'] == 1) {

            $get_button .= '<a onclick="mutasi_anggota($1,$2)" ><button type="button" class="btn btn-primary btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Mutasi Anggota"><i class="ti-shift-right" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Mutasi Anggota NonAktif (*hub petugas)"><i class="ti-printer" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

    public function button_pet_ktp($usr = '') {

        $get_button = "";

        if ($usr['role_admin'] == 0) {
            
        } else {
            if ($usr['delete_prev'] == 1) {
                $get_button .= '<a onclick="act_del_ktp($1)" ><button type="button" class="btn btn-danger btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Hapus Anggota"><i class="ti-close" aria-hidden="true"></i></button></a>';
            } else {
                $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Hapus KTP NonAktif (*hub petugas)"><i class="ti-close" aria-hidden="true"></i></button>';
            }
        }
        if ($usr['update_prev'] == 1) {
            $get_button .= '<a href="' . site_url('ktp/get_ktp_pet/') . '$1"><button type="button" class="btn btn-info btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Edit Anggota"><i class="ti-pencil" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Edit KTP NonAktif (*hub petugas)"><i class="ti-pencil" aria-hidden="true"></i></button>';
        }
        if ($usr['update_prev'] == 1 or $usr['create_prev'] == 1) {
            $get_button .= '<a target="_blank" href="' . site_url('laporan/cetak_kta/') . '$1"><button type="button" class="btn btn-warning btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Cetak KTA Anggota"><i class="ti-printer" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Cetak KTA NonAktif (*hub petugas)"><i class="ti-printer" aria-hidden="true"></i></button>';
        }
        if ($usr['update_prev'] == 1 or $usr['create_prev'] == 1) {

            $get_button .= '<a onclick="mutasi_anggota($1,$2)" ><button type="button" class="btn btn-primary btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Mutasi Anggota"><i class="ti-shift-right" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Mutasi Anggota NonAktif (*hub petugas)"><i class="ti-printer" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

    //-----------------------------------------------PETUGAS--------------------------------------------------//

    public function petugas_json() {

        if (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 2)) {

            $this->datatables->select("p.id_petugas,
                                    p.id_admin,
                                    UCASE(p.nama_petugas) AS nama_petugas,
                                    p.nomor_hp,
                                    p.alamat_petugas,
                                    p.email_petugas,
                                    p.nomor_ktp,
                                    p.kode_petugas,
                                    wp.nama AS provinsi,
                                    wkb.nama AS kabupaten,                                   
                                    IF(wkb.nama IS NULL,UCASE(wp.nama),CONCAT(UCASE(wp.nama),'-',UCASE(wkb.nama))) AS regional_petugas,
                                   (
                                    SELECT
                                        COUNT(k.id_ktp)
                                    FROM
                                        ktp k
                                    WHERE
                                        k.id_petugas = p.id_petugas AND k.status_data = 1
                                    ) AS jml_ktp,
                                    p.status_data,
                                    p.tanggal_post");
            $this->datatables->from('petugas p');
            $this->datatables->join('wilayah_kabupaten wkb', 'SUBSTR(p.id_admin, 3, 2) = wkb.id AND SUBSTR(p.id_admin, 1, 2) = wkb.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wp', 'SUBSTR(p.id_admin, 1, 2) = wp.id', 'left');
            $this->datatables->where('p.id_admin', $this->user['id_ref']);
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_nama_petugas', '<span class="label label-info"><b>$1</b></span>', 'nama_petugas');
            $this->datatables->add_column('view_kode_petugas', '<span class="label label-danger"><b>$1</b></span>', 'kode_petugas');
            $this->datatables->add_column('view_regional_petugas', '<span class="label label-success"><b>$1</b></span>', 'regional_petugas');
            $this->datatables->add_column('view_button', $this->button_petugas($this->user), 'id_petugas');
        } elseif (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 1)) {

            $this->datatables->select("p.id_petugas,
                                    p.id_admin,
                                    UCASE(p.nama_petugas) AS nama_petugas,
                                    p.nomor_hp,
                                    p.alamat_petugas,
                                    p.email_petugas,
                                    p.nomor_ktp,
                                    p.kode_petugas,
                                    wp.nama AS provinsi,
                                    wkb.nama AS kabupaten,                                   
                                    IF(wkb.nama IS NULL,UCASE(wp.nama),CONCAT(UCASE(wp.nama),'-',UCASE(wkb.nama))) AS regional_petugas,
                                   (
                                    SELECT
                                        COUNT(k.id_ktp)
                                    FROM
                                        ktp k
                                    WHERE
                                        k.id_petugas = p.id_petugas AND k.status_data = 1
                                    ) AS jml_ktp,
                                    p.status_data,
                                    p.tanggal_post");
            $this->datatables->from('petugas p');
            $this->datatables->join('wilayah_kabupaten wkb', 'SUBSTR(p.id_admin, 3, 2) = wkb.id AND SUBSTR(p.id_admin, 1, 2) = wkb.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wp', 'SUBSTR(p.id_admin, 1, 2) = wp.id', 'left');
            $this->datatables->like('p.id_admin', $this->user['id_ref'], 'after');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_nama_petugas', '<span class="label label-info"><b>$1</b></span>', 'nama_petugas');
            $this->datatables->add_column('view_kode_petugas', '<span class="label label-danger"><b>$1</b></span>', 'kode_petugas');
            $this->datatables->add_column('view_regional_petugas', '<span class="label label-success"><b>$1</b></span>', 'regional_petugas');
            $this->datatables->add_column('view_button', $this->button_petugas($this->user), 'id_petugas');
        } elseif ($this->user['role_admin'] == 0) {

            $this->datatables->select("p.id_petugas,
                                    p.id_admin,
                                    UCASE(p.nama_petugas) AS nama_petugas,
                                    p.nomor_hp,
                                    p.alamat_petugas,
                                    p.email_petugas,
                                    p.nomor_ktp,
                                    p.kode_petugas,
                                    wp.nama AS provinsi,
                                    wkb.nama AS kabupaten,                                   
                                    IF(wkb.nama IS NULL,UCASE(wp.nama),CONCAT(UCASE(wp.nama),'-',UCASE(wkb.nama))) AS regional_petugas,
                                   (
                                    SELECT
                                        COUNT(k.id_ktp)
                                    FROM
                                        ktp k
                                    WHERE
                                        k.id_petugas = p.id_petugas AND k.status_data = 1
                                    ) AS jml_ktp,
                                    p.status_data,
                                    p.tanggal_post");
            $this->datatables->from('petugas p');
            $this->datatables->join('wilayah_kabupaten wkb', 'SUBSTR(p.id_admin, 3, 2) = wkb.id AND SUBSTR(p.id_admin, 1, 2) = wkb.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wp', 'SUBSTR(p.id_admin, 1, 2) = wp.id', 'left');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_nama_petugas', '<span class="label label-info"><b>$1</b></span>', 'nama_petugas');
            $this->datatables->add_column('view_kode_petugas', '<span class="label label-danger"><b>$1</b></span>', 'kode_petugas');
            $this->datatables->add_column('view_regional_petugas', '<span class="label label-success"><b>$1</b></span>', 'regional_petugas');
            $this->datatables->add_column('view_button', $this->button_petugas($this->user), 'id_petugas');
        }

        return print_r($this->datatables->generate());
    }

    public function button_petugas($usr = '') {

        $get_button = "";

        if ($usr['role_admin'] == 0) {
            
        } else {
            if ($usr['delete_prev'] == 1) {
                $get_button .= '<a onclick="act_del_petugas($1)" ><button type="button" class="btn btn-danger btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Hapus Petugas"><i class="ti-close" aria-hidden="true"></i></button></a>';
            } else {
                $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Hapus Petugas NonAktif (*hub petugas)"><i class="ti-close" aria-hidden="true"></i></button>';
            }
        }

        if ($usr['update_prev'] == 1) {
            $get_button .= '<a href="' . site_url('petugas/get_petugas/') . '$1" ><button type="button" data-toggle="tooltip" class="btn btn-info btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-original-title="Edit Petugas"><i class="ti-pencil" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Edit Petugas NonAktif (*hub petugas)"><i class="ti-pencil" aria-hidden="true"></i></button>';
        }

        $get_button .= '<a href="' . site_url('ktp/lihat_ktp_pet/') . '$1"><button type="button" class="btn btn-warning btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Lihat Data KTP Petugas"><i class="ti-eye" aria-hidden="true"></i></button></a>';

        if ($usr['create_prev'] == 1) {
            $get_button .= '<a href="' . site_url('ktp/tambah_ktp_pet/') . '$1"><button type="button" class="btn btn-success btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Tambah KTP per Petugas"><i class="ti-plus" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Tambah KTP per Petugas NonAktif (*hub petugas)"><i class="ti-plus" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

    //-----------------------------------------------AKUN ADMIN KAB--------------------------------------------------//

    public function akun_kab_json() {

        if (!empty(substr($this->user['id_ref'], 0, 2)) && ($this->user['role_admin'] == 1)) {

            $this->datatables->select("u.id_user,
                                        u.id_ref,
                                        UCASE(u.nama_admin) AS nama_admin,
                                        u.status,
                                        u.email,
                                        u.nomor_hp,                                       
                                        DATE_FORMAT(STR_TO_DATE(u.license_exp, '%d/%m/%y'), '%Y-%m-%d') as license_exp,                                        
                                        u.status_data,
                                        u.create_prev,
                                        u.read_prev,
                                        u.update_prev,
                                        u.delete_prev,
                                        u.tanggal_post,
                                        UCASE(wp.nama) AS provinsi,
                                        UCASE(wkb.nama) AS kabupaten,    
                                        (
                                            SELECT
                                                COUNT(p.id_petugas)
                                            FROM
                                                petugas p
                                            WHERE
                                                p.id_admin = u.id_ref
                                        ) AS jml_pet,
                                        (
                                            SELECT
                                                COUNT(k.id_ktp)
                                            FROM
                                                ktp k
                                            WHERE
                                                k.id_admin = u.id_ref
                                        ) AS jml_ktp");
            $this->datatables->from('user u');
            $this->datatables->join('wilayah_kabupaten wkb', 'SUBSTR(u.id_ref, 3, 2) = wkb.id AND SUBSTR(u.id_ref, 1, 2) = wkb.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wp', 'SUBSTR(u.id_ref, 1, 2) = wp.id', 'left');
            $this->datatables->where('u.role_admin', '2');
            $this->datatables->where('SUBSTR(u.id_ref, 1, 2)', substr($this->user['id_ref'], 0, 2));

            $this->datatables->order_by('u.tanggal_post DESC');
        } elseif ($this->user['role_admin'] == 0) {

            $this->datatables->select("u.id_user,
                                        u.id_ref,
                                        UCASE(u.nama_admin) AS nama_admin,
                                        u.status,
                                        u.email,
                                        u.nomor_hp,                                       
                                        DATE_FORMAT(STR_TO_DATE(u.license_exp, '%d/%m/%y'), '%Y-%m-%d') as license_exp,                                        
                                        u.status_data,
                                        u.create_prev,
                                        u.read_prev,
                                        u.update_prev,
                                        u.delete_prev,
                                        u.tanggal_post,
                                        UCASE(wp.nama) AS provinsi,
                                        UCASE(wkb.nama) AS kabupaten,  
                                        (
                                            SELECT
                                                COUNT(p.id_petugas)
                                            FROM
                                                petugas p
                                            WHERE
                                                p.id_admin = u.id_ref
                                        ) AS jml_pet,
                                        (
                                            SELECT
                                                COUNT(k.id_ktp)
                                            FROM
                                                ktp k
                                            WHERE
                                                k.id_admin = u.id_ref
                                        ) AS jml_ktp");
            $this->datatables->from('user u');
            $this->datatables->join('wilayah_kabupaten wkb', 'SUBSTR(u.id_ref, 3, 2) = wkb.id AND SUBSTR(u.id_ref, 1, 2) = wkb.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wp', 'SUBSTR(u.id_ref, 1, 2) = wp.id', 'left');
            $this->datatables->where('u.role_admin', '2');
            $this->datatables->order_by('u.tanggal_post DESC');

            $this->datatables->add_column('view_button', $this->button_admin_kab($this->user), 'id_user, id_ref');
        }

        return print_r($this->datatables->generate());
    }

    public function button_admin_kab($usr = '') {

        $get_button = "";

        $get_button .= '<a onclick="act_del_admin_kab($1)" ><button type="button" class="btn btn-danger btn-sm btn-icon btn-pure btn-outline delete-row-btn " data-toggle="tooltip" data-original-title="Hapus Admin Kabupaten"><i class="ti-close" aria-hidden="true"></i></button></a>';
        $get_button .= '<a href="' . site_url('akun/get_admin_kabupaten/') . '$1" ><button type="button" data-toggle="tooltip" class="btn btn-info btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-original-title="Edit Admin Kabupaten"><i class="ti-pencil" aria-hidden="true"></i></button></a>';
        $get_button .= '<a href="' . site_url('ktp/lihat_ktp_admin/') . '$2" ><button type="button" data-toggle="tooltip" class="btn btn-warning btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-original-title="Lihat Anggota Admin Kabupaten"><i class="ti-eye" aria-hidden="true"></i></button></a>';

        if ($usr['create_prev'] == 1) {
            $get_button .= '<a href="' . site_url('ktp/tambah_ktp_admin/') . '$2"><button type="button" class="btn btn-success btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Tambah KTP Admin"><i class="ti-plus" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Tambah KTP Admin NonAktif (*hub petugas)"><i class="ti-plus" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

    //-----------------------------------------------KELOLA ANGGOTA--------------------------------------------------//

    public function kelola_anggota_json() {

        if (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 2)) {
            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir,                                    
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas AND p.id_admin = '" . $this->user['id_ref'] . "' AND p.status_data = 1 AND pt.status_data = 1 AND pt.id_admin = '" . $this->user['id_ref'] . "'", 'left');
            $this->datatables->where('p.id_admin', $this->user['id_ref']);
            $this->datatables->where('p.status_data', '1');
            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            $this->datatables->add_column('view_button', $this->button_ktp($this->user), 'id_ktp, status_mutasi');

            $this->datatables->order_by('p.tanggal_post DESC');
        } elseif (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 1)) {

            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    p.nama_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir,                                
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas AND p.id_admin LIKE '" . $this->user['id_ref'] . "%' AND p.status_data = 1 AND pt.status_data = 1 AND pt.id_admin LIKE '" . $this->user['id_ref'] . "%'", 'left');
            $this->datatables->like('p.id_admin', $this->user['id_ref'], 'after');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            $this->datatables->add_column('view_button', $this->button_ktp($this->user), 'id_ktp, status_mutasi');

            $this->datatables->order_by('p.tanggal_post DESC');
        } elseif ($this->user['role_admin'] == 0) {

            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    p.nama_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir, 
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas", 'left');
            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            $this->datatables->add_column('view_button', $this->button_delete_kelola_anggota($this->user), 'id_ktp');

            $this->datatables->order_by('p.tanggal_post DESC');
        }
        return print_r($this->datatables->generate());
    }

    public function button_delete_kelola_anggota($usr = '') {

        $get_button = "";

        if ($usr['delete_prev'] == 1) {
            $get_button .= '<a onclick="act_del_ktp($1)" ><button type="button" class="btn btn-danger btn-sm btn-icon btn-pure btn-outline delete-row-btn " data-toggle="tooltip" data-original-title="Hapus Anggota"><i class="ti-close" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn" readonly data-toggle="tooltip" data-original-title="Hapus KTP NonAktif (*hub petugas)"><i class="ti-close" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

    //-----------------------------------------------KELOLA PETUGAS--------------------------------------------------//

    public function kelola_petugas_json() {

        if (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 2)) {

            $this->datatables->select("p.id_petugas,
                                    p.id_admin,
                                    UCASE(p.nama_petugas) AS nama_petugas,
                                    p.nomor_hp,
                                    p.alamat_petugas,
                                    p.email_petugas,
                                    p.nomor_ktp,
                                    p.kode_petugas,
                                    wp.nama AS provinsi,
                                    wkb.nama AS kabupaten,                                   
                                    IF(wkb.nama IS NULL,UCASE(wp.nama),CONCAT(UCASE(wp.nama),'-',UCASE(wkb.nama))) AS regional_petugas,
                                   (
                                    SELECT
                                        COUNT(k.id_ktp)
                                    FROM
                                        ktp k
                                    WHERE
                                        k.id_petugas = p.id_petugas AND k.status_data = 1
                                    ) AS jml_ktp,
                                    p.status_data,
                                    p.tanggal_post");
            $this->datatables->from('petugas p');
            $this->datatables->join('wilayah_kabupaten wkb', 'SUBSTR(p.id_admin, 3, 2) = wkb.id AND SUBSTR(p.id_admin, 1, 2) = wkb.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wp', 'SUBSTR(p.id_admin, 1, 2) = wp.id', 'left');
            $this->datatables->where('p.id_admin', $this->user['id_ref']);
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_nama_petugas', '<span class="label label-info"><b>$1</b></span>', 'nama_petugas');
            $this->datatables->add_column('view_kode_petugas', '<span class="label label-danger"><b>$1</b></span>', 'kode_petugas');
            $this->datatables->add_column('view_regional_petugas', '<span class="label label-success"><b>$1</b></span>', 'regional_petugas');
            $this->datatables->add_column('view_button', $this->button_petugas($this->user), 'id_petugas');
        } elseif (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 1)) {

            $this->datatables->select("p.id_petugas,
                                    p.id_admin,
                                    UCASE(p.nama_petugas) AS nama_petugas,
                                    p.nomor_hp,
                                    p.alamat_petugas,
                                    p.email_petugas,
                                    p.nomor_ktp,
                                    p.kode_petugas,
                                    wp.nama AS provinsi,
                                    wkb.nama AS kabupaten,                                   
                                    IF(wkb.nama IS NULL,UCASE(wp.nama),CONCAT(UCASE(wp.nama),'-',UCASE(wkb.nama))) AS regional_petugas,
                                   (
                                    SELECT
                                        COUNT(k.id_ktp)
                                    FROM
                                        ktp k
                                    WHERE
                                        k.id_petugas = p.id_petugas AND k.status_data = 1
                                    ) AS jml_ktp,
                                    p.status_data,
                                    p.tanggal_post");
            $this->datatables->from('petugas p');
            $this->datatables->join('wilayah_kabupaten wkb', 'SUBSTR(p.id_admin, 3, 2) = wkb.id AND SUBSTR(p.id_admin, 1, 2) = wkb.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wp', 'SUBSTR(p.id_admin, 1, 2) = wp.id', 'left');
            $this->datatables->like('p.id_admin', $this->user['id_ref'], 'after');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_nama_petugas', '<span class="label label-info"><b>$1</b></span>', 'nama_petugas');
            $this->datatables->add_column('view_kode_petugas', '<span class="label label-danger"><b>$1</b></span>', 'kode_petugas');
            $this->datatables->add_column('view_regional_petugas', '<span class="label label-success"><b>$1</b></span>', 'regional_petugas');
            $this->datatables->add_column('view_button', $this->button_petugas($this->user), 'id_petugas');
        } elseif ($this->user['role_admin'] == 0) {

            $this->datatables->select("p.id_petugas,
                                    p.id_admin,
                                    UCASE(p.nama_petugas) AS nama_petugas,
                                    p.nomor_hp,
                                    p.alamat_petugas,
                                    p.email_petugas,
                                    p.nomor_ktp,
                                    p.kode_petugas,
                                    wp.nama AS provinsi,
                                    wkb.nama AS kabupaten,                                   
                                    IF(wkb.nama IS NULL,UCASE(wp.nama),CONCAT(UCASE(wp.nama),'-',UCASE(wkb.nama))) AS regional_petugas,
                                   (
                                    SELECT
                                        COUNT(k.id_ktp)
                                    FROM
                                        ktp k
                                    WHERE
                                        k.id_petugas = p.id_petugas AND k.status_data = 1
                                    ) AS jml_ktp,
                                    p.status_data,
                                    p.tanggal_post");
            $this->datatables->from('petugas p');
            $this->datatables->join('wilayah_kabupaten wkb', 'SUBSTR(p.id_admin, 3, 2) = wkb.id AND SUBSTR(p.id_admin, 1, 2) = wkb.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wp', 'SUBSTR(p.id_admin, 1, 2) = wp.id', 'left');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_nama_petugas', '<span class="label label-info"><b>$1</b></span>', 'nama_petugas');
            $this->datatables->add_column('view_kode_petugas', '<span class="label label-danger"><b>$1</b></span>', 'kode_petugas');
            $this->datatables->add_column('view_regional_petugas', '<span class="label label-success"><b>$1</b></span>', 'regional_petugas');
            $this->datatables->add_column('view_button', $this->button_delete_kelola_petugas($this->user), 'id_petugas');
        }

        return print_r($this->datatables->generate());
    }

    public function button_delete_kelola_petugas($usr = '') {

        $get_button = "";

        if ($usr['delete_prev'] == 1) {
            $get_button .= '<a onclick="act_del_petugas($1)" ><button type="button" class="btn btn-danger btn-sm btn-icon btn-pure btn-outline delete-row-btn " data-toggle="tooltip" data-original-title="Hapus Anggota"><i class="ti-close" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn" readonly data-toggle="tooltip" data-original-title="Hapus KTP NonAktif (*hub petugas)"><i class="ti-close" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

    //-----------------------------------------------ANGGOTA--------------------------------------------------//

    public function anggota_json($id = '') {

        $get_label = $this->Datatablemodel->get_label($id);

        if (!empty($get_label)) {
            $string_kab = explode(',', $get_label[0]->kabupaten);
            $string_kec = explode(',', $get_label[0]->kecamatan);
        }

        if (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 2) && !empty($get_label)) {

            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir,                                    
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas AND p.id_admin = '" . $this->user['id_ref'] . "' AND p.status_data = 1 AND pt.status_data = 1 AND pt.id_admin = '" . $this->user['id_ref'] . "'", 'left');
            $this->datatables->where('p.id_admin', $this->user['id_ref']);
            $this->datatables->where('SUBSTRING(p.id_asal,1,2)', $get_label[0]->provinsi);
            $this->datatables->where_in('SUBSTRING(p.id_asal,1,4)', $string_kab);
            if ($get_label[0]->kecamatan != null or ! empty($get_label[0]->kecamatan) or $get_label[0]->kecamatan != '') {
                $this->datatables->where_in('SUBSTRING(p.id_asal,1,6)', $string_kec);
            }
            $this->datatables->where('p.status_data', '1');
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            //$this->datatables->add_column('view_button', $this->button_anggota($this->user), 'id_ktp, status_mutasi');
        } elseif (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 1) && !empty($get_label)) {

            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    p.nama_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir,                                
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas AND p.id_admin LIKE '" . $this->user['id_ref'] . "%' AND p.status_data = 1 AND pt.status_data = 1 AND pt.id_admin LIKE '" . $this->user['id_ref'] . "%'", 'left');
            $this->datatables->like('p.id_admin', $this->user['id_ref'], 'after');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->where('SUBSTRING(p.id_asal,1,2)', $get_label[0]->provinsi);
            $this->datatables->where_in('SUBSTRING(p.id_asal,1,4)', $string_kab);
            if ($get_label[0]->kecamatan != null or ! empty($get_label[0]->kecamatan) or $get_label[0]->kecamatan != '') {
                $this->datatables->where_in('SUBSTRING(p.id_asal,1,6)', $string_kec);
            }
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            //$this->datatables->add_column('view_button', $this->button_anggota($this->user), 'id_ktp, status_mutasi');
        } elseif (empty($get_label)) {

            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    p.nama_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir, 
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas", 'left');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->where('p.id_asal', 0);
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
        } elseif (($this->user['role_admin'] == 0) && !empty($get_label)) {

            $this->datatables->select("p.id_ktp,
                                    p.nik_kta_baru,
                                    p.id_asal,
                                    p.id_admin,
                                    p.id_petugas,
                                    wpasl.nama AS provinsi_asal,
                                    wkbasl.nama AS kabupaten_asal,
                                    wkasl.nama AS kecamatan_asal,
                                    wdasl.nama AS kelurahan_asal,
                                    wpagt.nama AS provinsi_anggota,
                                    wkbagt.nama AS kabupaten_anggota,
                                    pt.nama_petugas,
                                    p.nik_ktp,
                                    p.nama_ktp,
                                    UCASE(p.nama_ktp) AS nama_ktp,
                                    CONCAT(UCASE(p.tempat_lahir),', ',p.tanggal_lahir) AS tmp_tgl_lahir, 
                                    IF(p.jenis_kelamin='1','LAKI-LAKI', 'PEREMPUAN') AS jenis_kelamin,
                                    p.gol_darah,
                                    p.alamat_ktp,
                                    p.agama,
                                    p.status_nikah,
                                    p.pekerjaan,
                                    p.nomor_hp_ktp,
                                    p.img,
                                    p.img_thumb,
                                    p.status_data,
                                    p.status_mutasi,
                                    p.tanggal_post");
            $this->datatables->from('ktp p');
            $this->datatables->join('wilayah_kabupaten wkbagt', 'SUBSTR(p.id_admin, 3, 2) = wkbagt.id AND SUBSTR(p.id_admin, 1, 2) = wkbagt.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpagt', 'SUBSTR(p.id_admin, 1, 2) = wpagt.id', 'left');
            $this->datatables->join('wilayah_desa wdasl', 'SUBSTR(p.id_asal, 7, 2) = wdasl.id AND SUBSTR(p.id_asal, 1, 2) = wdasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wdasl.id_dati2 AND SUBSTR(p.id_asal, 5, 2) = wdasl.id_dati3', 'left');
            $this->datatables->join('wilayah_kecamatan wkasl', 'SUBSTR(p.id_asal, 5, 2) = wkasl.id AND SUBSTR(p.id_asal, 1, 2) = wkasl.id_dati1 AND SUBSTR(p.id_asal, 3, 2) = wkasl.id_dati2', 'left');
            $this->datatables->join('wilayah_kabupaten wkbasl', 'SUBSTR(p.id_asal, 3, 2) = wkbasl.id AND SUBSTR(p.id_asal, 1, 2) = wkbasl.id_dati1', 'left');
            $this->datatables->join('wilayah_provinsi wpasl', 'SUBSTR(p.id_asal, 1, 2) = wpasl.id', 'left');
            $this->datatables->join('petugas pt', "p.id_petugas = pt.id_petugas", 'left');
            $this->datatables->where('p.status_data', '1');
            $this->datatables->where('SUBSTRING(p.id_asal,1,2)', $get_label[0]->provinsi);
            $this->datatables->where_in('SUBSTRING(p.id_asal,1,4)', $string_kab);
            if ($get_label[0]->kecamatan != null or ! empty($get_label[0]->kecamatan) or $get_label[0]->kecamatan != '') {
                $this->datatables->where_in('SUBSTRING(p.id_asal,1,6)', $string_kec);
            }
            $this->datatables->order_by('p.tanggal_post DESC');

            $this->datatables->add_column('view_ktp', '<a href="' . site_url('ktp/detail_ktp/') . '$1"data-toggle="tooltip" data-original-title="Lihat Detail KTP"><span class="label label-warning"><b>$2</b></span></a>', 'id_ktp, nik_ktp');
            $this->datatables->add_column('view_kta', '<span class="label label-info"><b>$1</b></span>', 'nik_kta_baru');
            // $this->datatables->add_column('view_button', $this->button_anggota($this->user), 'id_ktp, status_mutasi');
        }
        return print_r($this->datatables->generate());
    }

    public function button_anggota($usr = '') {

        $get_button = "";

        if ($usr['update_prev'] == 1 or $usr['create_prev'] == 1) {
            $get_button .= '<a target="_blank" href="' . site_url('laporan/cetak_kta_ktp/') . '$1"><button type="button" class="btn btn-warning btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Cetak KTA Anggota"><i class="ti-printer" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Cetak KTA NonAktif (*hub petugas)"><i class="ti-printer" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

    //-----------------------------------------------ANGGOTA--------------------------------------------------//

    public function format_json() {

        if (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 2)) {

            $this->datatables->select("f.id_format,
                                    f.id_admin,
                                    UCASE(f.nama_alias) AS nama_alias,
                                    f.kecamatan,
                                    wp.nama AS provinsi_nama,
                                    f.kategori_dapil,
                                    f.provinsi,
                                    f.kabupaten,
                                    f.kecamatan,                                                           
                                    f.tanggal_post");
            $this->datatables->from('format_cetak f');
            $this->datatables->join('wilayah_provinsi wp', 'wp.id=f.provinsi', 'left');
            $this->datatables->where_in('f.kabupaten', $this->user['id_ref']);
            $this->datatables->where_in('f.kategori_dapil', 3);
            $this->datatables->order_by('f.tanggal_post DESC');

            $this->datatables->add_column('view_button', $this->button_format($this->user), 'id_format');
        } elseif (!empty($this->user['id_ref']) && ($this->user['role_admin'] == 1)) {

            $this->datatables->select("f.id_format,
                                    f.id_admin,
                                    UCASE(f.nama_alias) AS nama_alias,
                                    f.kecamatan,
                                    wp.nama AS provinsi_nama,
                                    f.kategori_dapil,
                                    f.provinsi,
                                    f.kabupaten,
                                    f.kecamatan,                                                           
                                    f.tanggal_post");
            $this->datatables->from('format_cetak f');
            $this->datatables->where('f.provinsi', $this->user['id_ref']);
            $this->datatables->join('wilayah_provinsi wp', 'wp.id=f.provinsi', 'left');
            $this->datatables->order_by('f.tanggal_post DESC');

            $this->datatables->add_column('view_button', $this->button_format($this->user), 'id_format');
        } elseif ($this->user['role_admin'] == 0) {

            $this->datatables->select("f.id_format,
                                    f.id_admin,
                                    UCASE(f.nama_alias) AS nama_alias,
                                    f.kecamatan,
                                    wp.nama AS provinsi_nama,
                                    f.kategori_dapil,
                                    f.provinsi,
                                    f.kabupaten,
                                    f.kecamatan,                                                           
                                    f.tanggal_post");
            $this->datatables->from('format_cetak f');
            $this->datatables->join('wilayah_provinsi wp', 'wp.id=f.provinsi', 'left');
            $this->datatables->order_by('f.tanggal_post DESC');

            $this->datatables->add_column('view_button', $this->button_format($this->user), 'id_format');
        }
        return print_r($this->datatables->generate());
    }

    public function button_format($usr = '') {

        $get_button = "";

        if ($usr['update_prev'] == 1 or $usr['create_prev'] == 1) {
            $get_button .= '<a href="' . site_url('format/formatcetak/get_format/') . '$1"><button type="button" class="btn btn-info btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" data-toggle="tooltip" data-original-title="Edit Label Pencarian"><i class="ti-pencil" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn m-l-5" readonly data-toggle="tooltip" data-original-title="Edit Label NonAktif (*hub petugas)"><i class="ti-pencil" aria-hidden="true"></i></button>';
        }
        if ($usr['delete_prev'] == 1) {
            $get_button .= '<a onclick="act_del_format($1)" ><button type="button" class="btn btn-danger btn-sm btn-icon btn-pure btn-outline delete-row-btn  m-l-5" data-toggle="tooltip" data-original-title="Hapus Anggota"><i class="ti-close" aria-hidden="true"></i></button></a>';
        } else {
            $get_button .= '<button type="button" class="btn btn-default btn-sm btn-icon btn-pure btn-outline delete-row-btn  m-l-5" readonly data-toggle="tooltip" data-original-title="Hapus KTP NonAktif (*hub petugas)"><i class="ti-close" aria-hidden="true"></i></button>';
        }

        return $get_button;
    }

}
